<?php

/**
 * Administration settings form.
 *
 * @return
 *   The completed form definition.
 */
function basic_webmail_admin_settings() {
  $form = array();

  $form['basic_webmail_general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['basic_webmail_general_settings']['basic_webmail_account_info'] = array(
    '#type' => 'checkbox',
    '#title' => t("Use the user's account username and password hash for authenticating with the IMAP server."),
    '#default_value' => variable_get('basic_webmail_account_info', 1),
    '#description' => "Instead of providing additional form fields to get the user's e-mail address and password, just use what is provided within the account settings.",
  );
  $form['basic_webmail_general_settings']['basic_webmail_messages_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Messages per page'),
    '#size' => 10,
    '#default_value' => variable_get('basic_webmail_messages_per_page', 25),
    '#description' => t('The number of messages to show per page when viewing the listing.'),
  );
  $form['basic_webmail_general_settings']['basic_webmail_subject_characters'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of characters to display'),
    '#size' => 10,
    '#default_value' => variable_get('basic_webmail_subject_characters', 40),
    '#description' => t('This is the number of characters of the subject that are displayed in the message list before being truncated. Entering a zero (0) here will cause the listing to display all characters of the subject line, no matter how long it is.'),
  );
  $form['basic_webmail_general_settings']['basic_webmail_filter_format'] = filter_form(variable_get('basic_webmail_filter_format', FILTER_FORMAT_DEFAULT), NULL, array('basic_webmail_filter_format'));
  unset($form['basic_webmail_general_settings']['basic_webmail_filter_format']['#collapsed']);
  $form['basic_webmail_general_settings']['basic_webmail_filter_format']['#title'] = t('HTML view format');
  $timestamp = time();
  $small_date = format_date($timestamp, 'small');
  $medium_date = format_date($timestamp);
  $large_date = format_date($timestamp, $type = 'large');
  $options = array(
    'small' => t('Small (!small_date)',   array('!small_date' => $small_date)),
    'medium' => t('Medium (!medium_date)', array('!medium_date' => $medium_date)),
    'large' => t('Large (!large_date)',   array('!large_date' => $large_date)),
    'custom' => t('Custom'),
  );
  $form['basic_webmail_general_settings']['basic_webmail_format_option'] = array(
    '#type' => 'radios',
    '#title' => t('Format option'),
    '#options' => $options,
    '#default_value' => variable_get('basic_webmail_format_option', 'small'),
    '#description' => t('Specify how you want the date of the message displayed.'),
  );
  $form['basic_webmail_general_settings']['basic_webmail_custom_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom format'),
    '#default_value' => variable_get('basic_webmail_custom_format', 'D, M j, Y - g:i:s a'),
    '#description' => t('Specify how you want the Custom date format configured, using the format options of the PHP !date function.', array('!date' => l('date()', 'http://php.net/manual/en/function.date.php'))),
  );
  $form['basic_webmail_general_settings']['basic_webmail_number_attachments'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of attachments'),
    '#size' => 10,
    '#default_value' => variable_get('basic_webmail_number_attachments', 3),
    '#description' => t("The number of attachments to allow on the contact form. The maximum number of allowed uploads may be limited by PHP. If necessary, check your system's PHP php.ini file for a max_file_uploads directive to change."),
  );

  $form['basic_webmail_general_settings']['basic_webmail_attachment_size'] = array(
    '#type' => 'textfield', 
    '#title' => t('Maximum file size per upload'), 
    '#default_value' => variable_get('basic_webmail_attachment_size', 0), 
    '#size' => 5, 
    '#description' => t('The maximum file size a user can upload. If zero, server settings will be used.'), 
    '#field_suffix' => t('bytes'),
  );

  $form['basic_webmail_general_settings']['basic_webmail_attachment_extentions'] = array(
    '#type' => 'textfield', 
    '#title' => t('Permitted file extensions'), 
    '#default_value' => variable_get('basic_webmail_attachment_extentions', ''), 
    '#maxlength' => 255, 
    '#description' => t('Extensions that users can upload. Separate extensions with a space and do not include the leading dot. If not set, all extensions will be allowed.'),
  );

  $form['basic_webmail_server_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['basic_webmail_server_settings']['basic_webmail_server_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Server address'),
    '#default_value' => variable_get('basic_webmail_server_address', ''),
    '#description' => t('The address to the server you wish to connect to.'),
  );
  $form['basic_webmail_server_settings']['basic_webmail_server_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Connection port'),
    '#size' => 10,
    '#default_value' => variable_get('basic_webmail_server_port', 143),
    '#description' => t('The default IMAP port is 143 if one is not specified.'),
  );
  $form['basic_webmail_server_settings']['basic_webmail_secure_log_in'] = array(
    '#type' => 'checkbox',
    '#title' => t('Secure login.'),
    '#default_value' => variable_get('basic_webmail_secure_log_in', 0),
    '#description' => t('Check to make a secure connection to your IMAP Server.'),
  );
  $form['basic_webmail_server_settings']['basic_webmail_norsh'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the "norsh" flag'),
    '#default_value' => variable_get('basic_webmail_norsh', 0),
    '#description' => t('Do not use rsh or ssh to establish a preauthenticated IMAP session'),
  );
  $form['basic_webmail_server_settings']['basic_webmail_use_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Encrypt session using SSL.'),
    '#default_value' => variable_get('basic_webmail_use_ssl', 0),
    '#description' => t('Use SSL to connect to the server.'),
  );
  $form['basic_webmail_server_settings']['basic_webmail_validate_cert'] = array(
    '#type' => 'checkbox',
    '#title' => t('Validate certificate.'),
    '#default_value' => variable_get('basic_webmail_validate_cert', 0),
    '#description' => t('When using a secure connection, validate the certificate.'),
  );
  $form['basic_webmail_server_settings']['basic_webmail_use_tls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Encrypt session using TLS.'),
    '#default_value' => variable_get('basic_webmail_use_tls', 0),
    '#description' => t('Use TLS to connect to the server.'),
  );

  return system_settings_form($form);
}


/**
 * Additional vaidation for the administration settings form.
 *
 * @param form_id
 *   The form ID of the passed form.
 * @param $form_state
 *   The form values which you may perform validation on.
 */
function basic_webmail_admin_settings_validate($form, &$form_state) {
  if ($form_state['values']['basic_webmail_format_option'] == 'custom' && $form_state['values']['basic_webmail_custom_format'] == '') {
    form_set_error('basic_webmail_custom_format', t('You have specified Custom as your Format option for your Date format, but you have not specified the configuration of the Cuatom format. Either choose a different Format option, or specify the configuration of the Custom format.'));
  }

  if ($form_state['values']['basic_webmail_use_ssl'] == 1 && $form_state['values']['basic_webmail_server_port'] == '143') {
    form_set_error('basic_webmail_server_port', t('The normal port for secure IMAP is 993.'));
  }
}
