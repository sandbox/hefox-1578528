<?php

/**
 * Theme the message listing page.
 *
 * @param form
 *   The form to theme.
 *
 * @return
 *   The themed listing.
 */
function theme_basic_webmail_mail_list_form($form) {

  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Unread'),    'field' => 'message_unread'),
    array('data' => t('Att.'),   'field' => 'attachment_exists'),
    array('data' => t('Ans.'),  'field' => 'message_answered'),
    array('data' => t('Subject'),   'field' => 'message_subject'),
    array('data' => t('From'),      'field' => 'from_address'),
    array('data' => t('Date/Time'), 'field' => 'message_date',      'sort' => 'desc')
  );

  $output = drupal_render($form['options']);

  if (isset($form['message_subject']) && is_array($form['message_subject'])) {
    foreach (element_children($form['message_subject']) as $key) {
      $rows[] = array(
        drupal_render($form['messages'][$key]),
        drupal_render($form['message_unread'][$key]),
        drupal_render($form['attachment_exists'][$key]),
        drupal_render($form['message_answered'][$key]),
        drupal_render($form['message_subject'][$key]),
        drupal_render($form['from_address'][$key]),
        drupal_render($form['message_date'][$key]),
      );
    }
  }
  else  {
    $rows[] = array(array('data' => t('You have no e-mail.'), 'colspan' => '6'));
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}

/**
 * Theme the from line link.
 *
 * @param form
 *   The form to theme.
 *
 * @return
 *   The themed listing.
 */
function theme_basic_webmail_mail_link($email, $email_display) {
  return $email != $email_display ? l($email, 'basic_webmail/sendmail/' . $email) . ' &#60;' . check_plain($email_display) . '&#62;': l($email, 'basic_webmail/sendmail/' . $email);
}
