<?php


/**
 * Create the server connection string.
 *
 * @return
 *   The string representing the server connection information.
 */
function basic_webmail_get_server_string($folder = '') {
  static $server_string;

  if ($server_string) {
    return $server_string . $folder;
  }

  // Sanity check -- make sure the admin has done the configuration.
  if (!variable_get('basic_webmail_server_address', '') || !variable_get('basic_webmail_server_port', '')) {
    watchdog('basic_webmail', 'The <a href="@url">server settings</a> for Basic webmail have not yet been configured.', array('!server_settings' => url('admin/settings/basic_webmail')), WATCHDOG_ERROR);
    return FALSE;
  }

  // Prepare the flags for connecting to the server with.
  $server_string = variable_get('basic_webmail_server_address', '');
  $server_string .= variable_get('basic_webmail_server_port', '') != '143' ? ':' .  variable_get('basic_webmail_server_port', '') : '';
  $server_string .= '/imap';
  $server_string .= variable_get('basic_webmail_secure_log_in', 0) ? '/secure' : '';
  $server_string .= variable_get('basic_webmail_norsh', 0) ? '/norsh' : '';
  $server_string .= variable_get('basic_webmail_use_ssl', 0) ? '/ssl' : '';
  $server_string .= variable_get('basic_webmail_validate_cert', 0) ? '/validate-cert' : '/novalidate-cert';
  $server_string .= variable_get('basic_webmail_use_tls', 0) ? '/tls' : '/notls';

  $server_string = '{' . $server_string . '}';
  // Return the configured settings and create a connection string.
  return $server_string . imap_utf7_encode($folder);
}

/**
 * Connect to the mail server.
 *
 * @param folder
 *   The name of the folder to open. If no folder is specified, the default
 *   of INBOX is used.
 *
 * @return
 *   The mailbox connection, if successful, or FALSE if not.
 */
function basic_webmail_connection_load($folder = '', $account = NULL, $reset = FALSE) {
  $connections = &basic_webmail_connections();
  if (!$account) {
    global $user;
    $account = $user;
  }

  $key = $folder . '_' . $account->uid;
  if (!$reset && isset($connections[$key])) {
    return $connections[$key];
  }

  // If the account is empty, fail out (blank password is technically valid)
  if (empty($account->basic_webmail_username)){
    // @TODO Watchdog? could be called from anywhere, leading to confusion.
    drupal_set_message(t('Mail username not set, cannot connect.'), 'error', FALSE);
    return FALSE;
  }

  // Get the users e-mail login and password.
  $username = $account->basic_webmail_username;
  $password = $account->basic_webmail_password;

  if ($mailbox = basic_webmail_get_server_string($folder)) {
    $connections[$key] = version_compare(phpversion(), '5.2.0', '>=') ? imap_open($mailbox, $username, $password, 0, 1) : imap_open($mailbox, $username, $password, 0);
    if (!$connections[$key]) {
      drupal_set_message(t('Unable to connect to the server'), 'error', FALSE);
      watchdog('basic_webmail', 'There was an error accessing the remote server for folder @folder: !last_error', array('!last_error' => imap_last_error(), '@folder' => $folder ? $folder : 'none'), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  return $connections[$key];
}

/**
 * Keep track of currently open connections.
 */
function &basic_webmail_connections() {
  static $connections = array();
  return $connections;
}

/**
 * Closes any open connections.
 *
 * @param $mailbox_name
 *   Name of the mailbox to close connection for..
 * @param $account
 *   What account to close connection for.
 */
function basic_webmail_connection_close($folder = '', $account = NULL) {
  $connections = &basic_webmail_connections();
  if (!$account) {
    global $user;
    $account = $user;
  }
  if (isset($connections[$folder . '_' . $account->uid])) {
    if ($connections[$folder . '_' . $account->uid] != FALSE){
      imap_close($connections[$folder . '_' . $account->uid]);
    }
  }
  unset($connections[$folder . '_' . $account->uid]);
}

/**
 * Creates a mailbox for a user.
 *
 * @param $mailbox_name
 *   Name of the mailbox to create.
 * @param $account
 *   What account to create mailbox for.
 */
function basic_webmail_create_mailbox($mailbox_name, $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load('', $account)) {
    $server_string = basic_webmail_get_server_string($mailbox_name);
    if (imap_createmailbox($imap_resource, $server_string)) {
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error creating !box mailbox: !last_error', array('!last_error' => imap_last_error(), '!box' => $mailbox_name), WATCHDOG_WARNING);
    }
  }
  return FALSE;
}

/**
 * Deletes a mailbox for a user.
 *
 * @param $mailbox_name
 *   Name of the mailbox to create.
 * @param $account
 *   What account to create mailbox for.
 */
function basic_webmail_delete_mailbox($mailbox_name, $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load('', $account)) {
    $server_string = basic_webmail_get_server_string($mailbox_name);
    if (imap_deletemailbox($imap_resource, $server_string)) {
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error deleting !box mailbox: !last_error', array('!last_error' => imap_last_error(), '!box' => $mailbox_name), WATCHDOG_WARNING);
    }
  }
  return FALSE;
}

/**
 * Subscribes a user to a mailbox.
 *
 * @param $mailbox_name
 *   Name of the mailbox to create.
 * @param $account
 *   What account to create mailbox for.
 * @param $create
 *   Create the mailbox if it does not exist yet.
 */
function basic_webmail_mailbox_subscribe($mailbox_name, $account = NULL, $create = TRUE) {
  if ($imap_resource = basic_webmail_connection_load('', $account)) {
    $server_string = basic_webmail_get_server_string($mailbox_name);

    // Check if mailbox exists and creates if doesn't.
    if (!imap_status($imap_resource, $server_string, SA_ALL)) {
      if ($create) {
        if (!basic_webmail_create_mailbox($mailbox_name, $account)) {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    }

    // Try to subscribe to mailbox. Returns even if user is already subscribed.
    if (imap_subscribe($imap_resource, basic_webmail_get_server_string($mailbox_name))) {
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error subscribing to the !mailbox mailbox: !last_error', array('!mailbox' => $mailbox_name, '!last_error' => imap_last_error()), WATCHDOG_WARNING);
    }
  }
  return FALSE;
}

/**
 * Checks if the standard mailboxes exit and creates them, if necessary.
 *
 * This function checks to see if the standard mailboxes exit or not. If they
 * do not exist, this function will also create them. This function will then
 * subscribe to the mailboxes.
 */
function basic_webmail_subscribe_to_mailboxes($account = NULL) {
  $mailboxes = array_keys(basic_webmail_mailboxes());
  foreach ($mailboxes as $mailbox) {
    basic_webmail_mailbox_subscribe($mailbox, $account);
  }
  basic_webmail_connection_close('', $account);
}

/**
 * Parses a full mailbox name into the friendly shorter version.
 *
 * @param $mailbox_string
 *   The full mailbox string in format {server info}MAILBOX
 *
 * @return
 *  The parsed, readable mailbox name.
 */
function basic_webmail_parse_mailbox_name($mailbox_string) {
  return imap_utf7_decode(drupal_substr($mailbox_string, strpos($mailbox_string, '}') + 1));
}

/**
 * Returns what mailboxes exist for a given account.
 *
 * @param $account
 *   The user account to check for.
 *
 * @return
 *   Returns an array of mailboxes keyed by mailbox.
 */
function basic_webmail_get_mailboxes($account = NULL) {
  $mailboxes = array();
  if ($imap_resource = basic_webmail_connection_load('', $account)) {
    if ($_mailboxes = imap_list($imap_resource, basic_webmail_get_server_string(), '*')) {
      foreach ($_mailboxes as $mailbox) {
        $mailboxes[$mailbox] = basic_webmail_parse_mailbox_name($mailbox);
      }
    }
  }
  return $mailboxes;
}

/**
 * Returns what mailboxes exist for a given account.
 *
 * @param $account
 *   The user account to check for.
 *
 * @return
 *   Returns an array of mailboxes keyed by mailbox.
 */
function basic_webmail_get_mailboxes_subscribed($account = NULL) {
  $mailboxes = array();
  if ($imap_resource = basic_webmail_connection_load('', $account)) {
    if ($_mailboxes = imap_lsub($imap_resource, basic_webmail_get_server_string(), '*')) {
      foreach ($_mailboxes as $mailbox) {
        $mailboxes[$mailbox] = basic_webmail_parse_mailbox_name($mailbox);
      }
    }
  }
  return $mailboxes;
}


/**
 * Checks for the existence of a specified mailbox.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param mailbox
 *   The name of the mailbox to check for.
 *
 * @return
 *   Returns TRUE if the mailbox exists, and FALSE if it does not exist.
 */
function basic_webmail_mailbox_exists($mailbox_name, $account = NULL) {
  $mailboxes = basic_webmail_get_mailboxes($account);
  return in_array($mailbox_name, $mailboxes);
}

/**
 * Sends an imap message.
 *
 * @see imap_mail_compose().
 *
 * @param envelope
 *   An array of headers for message.
 * @param body
 *   An array of body for the message.
 *
 * @return
 *   The message sent, or false on failure.
 */
function basic_webmail_send_message($envelope, $body, $mailbox_name = 'INBOX', $to_account = NULL, $from_account = NULL, $options = array()) {
  if (!isset($to_account)) {
    global $user;
    $to_account = $user;
  }

  if (empty($envelope['from'])) {
    $envelope['from'] = $from_account->basic_webmail_username;
  }
  if (empty($envelope['to'])) {
    $envelope['to'] = $to_account->basic_webmail_username;
  }

  $envelope += array(
    'return_path' => $envelope['from'],
    'reply_to' => $envelope['from'],
  );

  $info = array(
    'envelope' => $envelope,
    'body' => $body,
    'to_account' => $to_account,
    'from_account' => $from_account,
    'options' => $options,
    'mailbox' => $mailbox_name,
  );
  drupal_alter('basic_webmail_message', $info);

  $sent_message = imap_mail_compose($info['envelope'], $info['body']);
  if (!$sent_message) {
    // Report the error.
    watchdog('basic_webmail', 'There was an error creating the copy a message: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_ERROR);
  }
  elseif ($imap_resource = basic_webmail_connection_load('', $to_account)) {
    // Move the message into given mailbox.
    $flags = isset($options['flags']) ? $options['flags'] : '';
    if (imap_append($imap_resource, basic_webmail_get_server_string($mailbox_name), $sent_message, $flags)) {
      module_invoke_all('basic_webmail_message_sent', $info);
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error saving a copy of the message to the folder: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_ERROR);
    }
  }
  return FALSE;
}


/**
 * Sends an imap message.
 *
 * @param $subject
 *   A string that titles the message.
 * @param body
 *   The content of the message as a string.
 * @param $to_account
 *   The account to send the message to
 * @param $from_account
 *   The account the message is from.
 *
 * @return
 *   The message sent, or false on failure.
 */
function basic_webmail_send_message_simple($subject, $body, $to_account, $from_account) {
  $envelope = array(
    'subject' => $subject,
  );
  $body = array(
    array(
      'type' => TYPETEXT,
      'subtype' => 'plain',
      'description' => 'description',
      '"contents.data' => $body
    ),
  );
  return basic_webmail_send_message($envelope, $body, 'INBOX', $to_account, $from_account);
}

/**
 * Wrapper around imap_uid and imap_msgno.
 *
 * @param $msgno
 *   Message number to retrieve a number for.
 * @param $uid
 *   A unique id to retrieve msg number for.
 */
function basic_webmail_get_msgnum_or_uid($mailbox_name, $msgno = NULL, $uid = NULL, $reset = FALSE, $account = NULL) {
  static $map = array();
  if ($reset) {
    $map = array();
  }
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    if (!isset($map[$mailbox_name])) {
      $map[$mailbox_name] = array();
    }

    if ($msgno) {
      if (!isset($map[$mailbox_name][$msgno])) {
        $map[$mailbox_name][$msgno] = imap_uid($imap_resource, $msgno);
      }
      return $map[$mailbox_name][$msgno];
    }
    if ($uid) {
      $pos = array_search($uid, $map[$mailbox_name]);
      if ($pos === FALSE) {
        $msgno = imap_msgno($imap_resource, $uid);
        $map[$mailbox_name][$msgno] = $uid;
        return $msgno;
      }
      return $pos;
    }
  }
}

/**
 * Wrapper around imap_msgno.
 */
function basic_webmail_msgno($mailbox_name, $uid, $account = NULL) {
  return basic_webmail_get_msgnum_or_uid($mailbox_name, NULL, $uid);
}

/**
 * Wrapper around imap_uid.
 */
function basic_webmail_uid($mailbox_name, $msgno) {
  return basic_webmail_get_msgnum_or_uid($mailbox_name, $msgno);
}


/**
 * Get a list of messages.
 *
 * @param $mailbox_name
 *   The mailbox to list messages from.
 * @param $start
 *   The begining of listing.
 * @param $max
 *   The max amount of messages to retrieve.
 * @param $sort
 *   The sort to process the messages.
 * @param $account
 *   The account to retrieve messages for.
 */
function basic_webmail_list_messages($mailbox_name = 'INBOX', $start = 0, $max = 10, $sort = 'ASC', $account = NULL) {
  $messages = array();
  if ($message_amount = basic_webmail_get_message_count($mailbox_name)) {
    if ($sort == 'ASC') {
      for ($i = $start + 1; $i <= $max && $i <= $message_amount; $i++) {
        if ($uid = basic_webmail_uid($mailbox_name, $i, $account)) {
          $messages[$uid] = basic_webmail_message_load($mailbox_name, $uid);
        }
      }
    }
    else {
      for ($i = $message_amount - $start; $i > ($message_amount - $start - $max) && $i > 0; $i--) {
        if ($uid = basic_webmail_uid($mailbox_name, $i, $account)) {
          $messages[$uid] = basic_webmail_message_load($mailbox_name, $uid);
        }
      }
    }
  }
  return $messages;
}

/**
 * Retrieve how many total messages are in a box.
 */
function basic_webmail_get_message_count($mailbox_name = 'INBOX', $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    imap_check($imap_resource);
    return imap_num_msg($imap_resource);
  }
  return 0;
}
/**
 * Get a list of messages.
 *
 * @param $mailbox_name
 *   The mailbox to list messages from.
 * @param $start
 *   The begining of listing.
 * @param $max
 *   The max amount of messages to retrieve.
 * @param $sort
 *   The sort to process the messages.
 * @param $account
 *   The account to retrieve messages for.
 */
function basic_webmail_get_mailbox_info($mailbox_name = 'INBOX', $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    if ($return = imap_status($imap_resource, basic_webmail_get_server_string($mailbox_name), SA_ALL)) {
      return $return;
    }
    watchdog('basic_webmail', 'Unable to retrieve information about mailbox: !last_error', array('!last_error' => $last_error), WATCHDOG_ERROR);
  }
  return FALSE;
}

/**
 * Returns the last message inserted.
 *
 * Warning: Due to delays in delivery, this isn't the last
 * message sent in some cases.
 *
 * @param $mailbox_name
 *   The mailbox name.
 * @param $account
 *   The account the message belongs to.
 */
function basic_webmail_last_message($mailbox_name = 'INBOX', $account = NULL) {
  return reset(basic_webmail_list_messages($mailbox_name, 0, 1, 'DESC', $account));
}

/**
 * Loads a given message
 *
 * @param $mailbox_name
 *   The mailbox name.
 * @param $uid
 *   The number (uid) of the message.
 * @param $full_message
 *   Load the full message instead of just the basic (header) info.
 * @param $account
 *   The account the message belongs to.
 * @param $reset
 *   Reset the internal cache, does not return a loaded message.
 *
 * @return
 *   An array of information about the message.
 */
function basic_webmail_message_load($mailbox_name, $uid, $full_message = FALSE, $account = NULL, $reset = FALSE) {
  static $messages = array(), $message_shorts = array();
  if (!$account) {
    $account = $GLOBALS['user'];
  }
  $message_cache_key = $mailbox_name . '_' . $uid . '_' . $account->uid;

  if ($reset) {
    unset($messages[$message_cache_key], $message_shorts[$message_cache_key]);
    return;
  }

  if ($full_message && !empty($messages[$message_cache_key])) {
    return $messages[$message_cache_key];
  }
  elseif (!$full_message && !empty($message_shorts[$message_cache_key])) {
    return $message_shorts[$message_cache_key];
  }

  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    if (empty($message_shorts[$message_cache_key])) {
      $message = array();
      $message['uid'] = $uid;
      $message_number = basic_webmail_msgno($mailbox_name, $uid, $account);
      $message['header'] = imap_headerinfo($imap_resource, $message_number);
      $message['header_822'] = imap_rfc822_parse_headers(imap_fetchheader($imap_resource, $message_number));
      if (empty($message['header'])) {
        return FALSE;
      }
      $message['deleted'] = $message['header']->Deleted == 'D';
      $message['recent'] = $message['header']->Recent == 'R' || $message['header']->Recent == 'N';
      $message['unseen'] = $message['header']->Unseen == 'U' || $message['header']->Recent == 'N';
      $message['flagged'] = $message['header']->Flagged == 'F';
      $message['answered'] = $message['header']->Answered == 'A';
      $message['draft'] = $message['header']->Draft == 'X';
      $message['path'] = 'basic_webmail/message/' . $mailbox_name . '/' . $uid;
      $message['display_subject'] = $message['subject'] = $message['header']->Subject ? basic_webmail_decode_mime_str($message['header']->Subject) : t('(No subject given.)');
      if ($number_characters = variable_get('basic_webmail_subject_characters', 40)) {
        $message['display_subject'] = truncate_utf8($message['subject'], $number_characters, TRUE, TRUE);
      }
      $message['message_number'] = $message_number;
      $message['mailbox_name'] = $mailbox_name;
      $message['id'] = $message['header']-->message_id;

      // Remove missing domain text returned from IMAP server
      if (variable_get('basic_webmail_account_info', 1)){
        foreach (array('toaddress', 'fromaddress', 'reply_toaddress', 'senderaddress') as $key) {
          if (!empty($message['header']->$key)) {
            $message['header']->$key = str_replace('@MISSING_DOMAIN', '', $message['header']->$key);
          }
        }
      }

      foreach (array('to', 'cc', 'from', 'bcc') as $key) {
        $message[$key] = array();
        if (!empty($message['header_822']->$key)) {
          foreach ($message['header_822']->$key as $email) {
            // Allow altering of mailbox name on output for each
            drupal_alter('basic_webmail_name_from_imap', $email->mailbox);
            $display = $email->mailbox;
            if (!variable_get('basic_webmail_account_info', 1)){
              $display.= '@' . $email->host;
            }
            $message[$key][] = array(
              'mail' => $display,
              'display_name' => !empty($email->personal) ? $email->personal : $display,
            );
          }
        }
      }
      if (empty($message['from'])) {
        $message['from'][0] = array('display_name' => t('Unknown'), 'mail' => t('unknown'));
      }

      $message['date'] = !empty($message['header']->MailDate) ? strtotime($message['header']->MailDate) : (!empty($message['header']->date) ? strtotime($message['header']->date) : 0);
      $message['date_formated'] = $message['date'] ? format_date($message['date'], variable_get('basic_webmail_format_option', 'small'), variable_get('basic_webmail_custom_format', 'D, M j, Y - g:i:s a')) : t('(Invalid date.)');

      // If there's more than just a single body part, then there's attachments!
      $message['parts'] = _basic_webmail_get_parts_list($imap_resource, $message_number);
      $message['attachments'] = count($message['parts']) > 1;

      $message_shorts[$message_cache_key] = $message;
    }
    else {
      $message = $message_shorts[$message_cache_key];
    }
    if ($full_message) {
      basic_webmail_message_full($imap_resource, $message);
      $messages[$message_cache_key] = $message;
    }
  }
  return $message ? $message : FALSE;
}

/**
 * Loads up body, etc.
 */
function basic_webmail_message_full($imap_resource, &$message) {
  // Get the message parts list.
  if ($parts_list = _basic_webmail_get_parts_list($imap_resource, $message['message_number'])) {
    $message['message_body_parts_raw'] = $parts_list;
    $message['message_body_parts'] = array();
    $message['message_PLAIN'] = '';
    $message['message_HTML'] = '';
    $message['attachments_links'] = array();
    // Iterate over the parts list.
    foreach ($parts_list as $part_id => $part_array) {
      // Process an HTML part.
      $type = drupal_strtoupper($part_array[0]);
      if (!empty($part_array[1])) {
        $name = t('Attachment');

        $params = (array) $part_array[1]->parameters;
        if (!empty($params['name'])){
          $name = $params['name'];
        }
        else if(!empty($part_array[1]->description)) {
          $name = $part_array[1]->description;
        }
        $message['attachments_links'][] = l($name, 'basic_webmail/message/' . $message['mailbox_name'] . '/' . $message['message_number'] . '/attachment/' . $part_id);
      }
      elseif ($type == 'HTML' || $type == 'PLAIN') {
        $message['message_body_parts'][$part_id] = array(
          'body' => _basic_webmail_decode_body_part($imap_resource, $message['message_number'], $part_id),
          'type' => $type,
          'part_id' => $part_id,
        );
        $message['message_' . $type] .= $message['message_body_parts'][$part_id]['body'] . "\n";
      }
    }
    $message['parts_list'] = $parts_list;
  }
}

/**
 * Decode the body part for the specified message number and message part ID.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param message_number
 *   The number of the message to get the information for.
 * @param part_id
 *   The ID of the part to get the information for.
 *
 * @return
 *   The message body, decoded if necessary.
 */
function _basic_webmail_decode_body_part($imap_resource, $message_number, $part_id) {
  $message_body_structure = imap_bodystruct($imap_resource, $message_number, $part_id);
  $part = _basic_webmail_mail_fetchpart($imap_resource, $message_number, $part_id);
  switch ($message_body_structure->encoding) {
    case 3:
      return imap_base64($part);
    case 4:
      return utf8_encode(quoted_printable_decode($part));
  }
  return $part;
}

// Get the body of a part of a message according to the string in $part.
function _basic_webmail_mail_fetchpart($imap_resource, $message_number, $part) {
  $parts = _basic_webmail_mail_fetchparts($imap_resource, $message_number);
  $part_numbers = explode(".", $part);
  $current_part = $parts;
  while (list($key, $val) = each($part_numbers)) {
    $current_part = $current_part[$val];
  }

  return $current_part ? $current_part : FALSE;
}


// Get an array with the bodies of all parts of an email.
// The structure of the array corresponds to the structure that is available
// with imap_fetchstructure.
// @TODO cache this stuff somehow.
function _basic_webmail_mail_fetchparts($imap_resource, $message_number) {
  static $all_parts;
  $parts = array();
  $header = imap_fetchheader($imap_resource, $message_number);
  $body = imap_body($imap_resource, $message_number, FT_INTERNAL);
  $i = 1;

  $new_parts = _basic_webmail_mail_mimesplit($header, $body);
  if ($new_parts) {
    while (list ($key, $val) = each($new_parts)) {
      $parts[$i] = _basic_webmail_mail_mimesub($val);
      $i++;
    }
  }
  else {
    $parts[$i] = $body;
  }
  return $parts;
}

// Returns an array with all parts that are subparts of the given part.
// If no subparts are found, returns the body of the current part.
function _basic_webmail_mail_mimesub($part) {
  $i= 1;
  $head_delimiter = "\r\n\r\n";
  $del_length = drupal_strlen($head_delimiter);

  // get head & body of the current part
  $end_of_head = strpos($part, $head_delimiter);
  $head = drupal_substr($part, 0, $end_of_head);
  $body = drupal_substr($part, $end_of_head + $del_length, drupal_strlen($part));

  // check whether it is a message according to rfc822
  if (stristr($head, "Content-Type: message/rfc822")) {
    $part = drupal_substr($part, $end_of_head + $del_length, drupal_strlen($part));
    $return_parts[1] = _basic_webmail_mail_mimesub($part);
    return $return_parts;
  }
  // if no message, get subparts and call function recursively
  elseif ($sub_parts = _basic_webmail_mail_mimesplit($head, $body)) {
    while (list ($key, $val) = each($sub_parts)) {
      $return_parts[$i] = _basic_webmail_mail_mimesub($val);
      $i++;
    }
    return $return_parts;
  }
  else {
    return $body;
  }
}


// Splits a message given in the body if it is a mulitpart mime message and
// returns the parts, if no parts are found, returns false.
function _basic_webmail_mail_mimesplit($header, $body) {
  $regs = array();
  $PN_EREG_BOUNDARY = "/Content-Type:(.*)boundary=(.+)/i";
  if (preg_match($PN_EREG_BOUNDARY, $header, $regs)) {
    $boundary = trim($regs[2], "\"\n\r'");
    if ($parts = preg_split("/([^\r\n]*)$boundary([^\r\n]*)/", $body)) {
      array_pop($parts);
      array_shift($parts);
      return array_values($parts);
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Get a list of all the possible parts.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param message_number
 *   The number of the message to get the information for.
 *
 * @return
 *   An array containing all of the available message parts, or FALSE if
 *   there is an error.
 */
function _basic_webmail_get_parts_list($imap_resource, $message_number) {
  // Initialize the return value.
  $parts_list = FALSE;

  // Retrieve the message structure.
  // Use FT_UID to ensure $message number is treated as unique message id (uid)
  $message_structure = imap_fetchstructure($imap_resource, $message_number, FT_UID);
  if (!$message_structure) {
    $last_error = imap_last_error();
    watchdog('basic_webmail', 'There was an error retrieving the message structure: !last_error', array('!last_error' => $last_error), WATCHDOG_ERROR);
    return FALSE;
  }

  // Pull the parts from the message structure.
  $parts = isset($message_structure->parts) ? $message_structure->parts  : FALSE;

  if (!$parts || $message_structure->type == 0) {
    // Simple message -- only 1 part.
    $parts_list[1] = array($message_structure->subtype);
  }
  else {
    // Complex message -- multiple parts.
    $endwhile = FALSE;
    // Stack while parsing message.
    $stack = array();
    // Incrementer.
    $i = 0;

    while (!$endwhile) {
      if (empty($parts[$i])) {
        if (count($stack) > 0) {
          $stack_count = count($stack) - 1;
          $parts = $stack[$stack_count]['p'];
          $i = $stack[$stack_count]['i'] + 1;
          array_pop($stack);
        }
        else {
          $endwhile = TRUE;
        }
      }

      if (!$endwhile) {
        // Create message part first (example '1.2.3').
        $partstring = '';

        foreach ($stack as $s) {
          $partstring .= ($s['i'] + 1) . '.';
        }

        $partstring .= ($i + 1);

        // Determine attachment status.
        // If ifdisposition == 1, then there's one or more attachments. If
        // type > 2 (0=plain & 1=html) then it's not a message attachment.
        if (!empty($parts[$i]->ifdisposition) && $parts[$i]->disposition == 'attachment') {
          // One or more attachments exists.
          $parts_list[$partstring] = array(drupal_strtoupper($parts[$i]->subtype), $parts[$i]);
        }
        else {
          // No attachments exist.
          $parts_list[$partstring] = array(drupal_strtoupper($parts[$i]->subtype));
        }
      }

      if (!empty($parts[$i]->parts)) {
        $stack[] = array('p' => $parts, 'i' => $i);
        $parts = $parts[$i]->parts;
        $i = 0;
      }
      else {
        $i++;
      }
    }
  }
  return $parts_list;
}

/**
 * Process the attachment for the specified message number and message part ID.
 *
 * @param imap_resource
 *   The IMAP stream, created by calling _basic_webmail_connect_to_server.
 * @param message_number
 *   The number of the message to get the information for.
 * @param part_id
 *   The ID of the part to get the information for.
 * @param part_structure
 *   An object with the structure definitaion of the part of the message that
 *   represents the attachment.
 *
 * @return
 *   An array containing all of the available message parts, or FALSE if
 *   there is an error.
 */
function _basic_webmail_process_attachment($imap_resource, $message_number, $part_id, $part_structure) {
  $attachment_info = array();
  $file_name = $part_structure->dparameters[0]->value;
  $file_data = _basic_webmail_mail_fetchpart($imap_resource, $message_number, $part_id);

  if ($part_structure->encoding == 3) {
    // Decode if base64.
    $file_data = base64_decode($file_data);
  }
  elseif ($part_structure->encoding == 4) {
    $file_data = quoted_printable_decode($file_data);
  }

  // Create the destination path variable, along with creating any necessary
  // directories.
  $file_dest = variable_get('basic_webmail_attachment_location', file_directory_path() . '/attachments');
  $file_dest .= '/';

  global $user;
  $file_dest .= $user->name;
  file_check_directory($file_dest, FILE_CREATE_DIRECTORY);
  $file_dest .= '/';

  $message_overview = imap_fetch_overview($imap_resource, arg(2));
  $file_dest .= drupal_substr($message_overview[0]->message_id, 1, drupal_strlen($message_overview[0]->message_id) - 2);
  file_check_directory($file_dest, FILE_CREATE_DIRECTORY);

  if (isset($_SESSION['messages']) && isset($_SESSION['messages']['status'])) {
    foreach ($_SESSION['messages']['status'] as $key => $drupal_message) {
      if (drupal_substr($drupal_message, 0, 13) == 'The directory' && drupal_substr($drupal_message, drupal_strlen($drupal_message) - 17, 17) == 'has been created.') {
        unset($_SESSION['messages']['status'][$key]);
      }
    }
  }

  $file_path = file_save_data($file_data, $file_dest . '/' . $file_name, FILE_EXISTS_REPLACE);
  $file_url = file_create_url($file_path);
  $attachment = array($file_path, $file_name, $file_url);

  return $attachment;
}

/**
 * Deletes a given message.
 *
 * @param $mailbox_name
 *   The mailbox name.
 * @param $uid
 *   The number (uid) of the message.
 * @param $account
 *   The account the message belongs to.
 * @param $force_delete
 *   If true, delete will be called instead of moving to trash first.
 * @param $expunge
 *   Clean out deleted/moved messages. Set to FALSE if looping.
 *
 * @return
 *  True on successful delete (or move to trash).
 */
function basic_webmail_message_delete($mailbox_name, $uid, $account = NULL, $force_delete = FALSE, $expunge = TRUE) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    $message_number = basic_webmail_msgno($mailbox_name, $uid, $account);
    $result = $mailbox_name != 'INBOX.Trash' && !$force_delete ? imap_mail_move($imap_resource, $message_number, 'INBOX.Trash') : imap_delete($imap_resource, $message_number);
    if ($result) {
      basic_webmail_message_load($mailbox_name, $uid, FALSE, $account, TRUE);
      // Clear out deleted email.
      if ($expunge && !basic_webmail_expunge_mailbox($mailbox_name)) {
        // Report the error.
        watchdog('basic_webmail', 'There was an error expunging the mailboxes: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_WARNING);
      }
      imap_check(basic_webmail_connection_load('INBOX.Trash', $account));
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error deleting the message: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_ERROR);
    }
  }
  return FALSE;
}


/**
 * Moves a given message.
 *
 * @param $mailbox_name
 *   The mailbox name.
 * @param $uid
 *   The number (uid) of the message.
 * @param $account
 *   The account the message belongs to.
 * @param $mailbox_target
 *   Target mailbox for moved message.
 * @param $expunge
 *   Clean out deleted/moved messages. Set to FALSE if looping.
 *
 * @return
 *  True on successful delete (or move to trash).
 */
function basic_webmail_message_move($mailbox_name, $uid, $mailbox_target, $account = NULL, $expunge = TRUE) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    $message_number = basic_webmail_msgno($mailbox_name, $uid, $account);
    if (imap_mail_move($imap_resource, $message_number, $mailbox_target)) {
      basic_webmail_message_load($mailbox_name, $uid, FALSE, $account, TRUE);
      // Clear out deleted email.
      if ($expunge && !basic_webmail_expunge_mailbox($mailbox_name)) {
        // Report the error.
        watchdog('basic_webmail', 'There was an error expunging the mailboxes: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_WARNING);
      }
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error moving the message: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_ERROR);
    }
  }
  return FALSE;
}

/**
 * Copies a given message.
 *
 * @param $mailbox_name
 *   The mailbox name.
 * @param $uid
 *   The number (uid) of the message.
 * @param $account
 *   The account the message belongs to.
 * @param $mailbox_target
 *   Target mailbox for copy message.
 * @param $expunge
 *   Clean out deleted/moved messages. Set to FALSE if looping.
 *
 * @return
 *  True on successful delete (or move to trash).
 */
function basic_webmail_message_copy($mailbox_name, $uid, $mailbox_target, $account = NULL, $expunge = TRUE) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    $message_number = basic_webmail_msgno($mailbox_name, $uid, $account);
    if (imap_mail_copy($imap_resource, $message_number, $mailbox_target)) {
      basic_webmail_message_load($mailbox_name, $uid, FALSE, $account, TRUE);
      // Clear out deleted email.
      if ($expunge && !basic_webmail_expunge_mailbox($mailbox_name)) {
        // Report the error.
        watchdog('basic_webmail', 'There was an error expunging the mailboxes: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_WARNING);
      }
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error moving the message: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_ERROR);
    }
  }
  return FALSE;
}

/**
 * Clears out any deleted/moved messages.
 *
 * @param $mailbox_name
 *   The mailbox name.
 * @param $account
 *   The account the message belongs to.
 *
 * @return
 *   TRUE on success, FALSE on failure.
 */
function basic_webmail_expunge_mailbox($mailbox_name, $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    if (imap_expunge($imap_resource)) {
      return TRUE;
    }
    watchdog('basic_webmail', 'There was an error expunging the mailboxes: !last_error', array('!last_error' => imap_last_error()), WATCHDOG_WARNING);
  }
  return FALSE;
}

/**
 * Sets the flag for a message
 *
 * @param $mailbox_name
 *   The name of the mailbox.
 * @param $message_sequence
 *   The message or sequence of messages to set a flag for.
 * @param $flags
 *   The type of flag to set. Possible flags:
 *    *\\Seen
 *    *\\Answered
 *    *\\Flagged
 *    *\\Deleted
 *    *\\Draft
 *   May be a single line seperated list of flags.
 * @param $account
 *   The account the message belongs to.
 */
function basic_webmail_mark_message($mailbox_name, $uid, $flags, $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    $message_number = basic_webmail_msgno($mailbox_name, $uid, $account);
    if (imap_setflag_full($imap_resource, $message_number, $flags)) {
      basic_webmail_message_load($mailbox_name, $uid, FALSE, $account, TRUE);
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error marking message @message_sequence as !flags: !last_error', array('!last_error' => imap_last_error(), '@message_sequence' => $message_number, '!flags' => str_replace('/', '', $flags)), WATCHDOG_ERROR);
    }
  }
  return FALSE;
}

/**
 * Removes the flag for a message
 *
 * @param $mailbox_name
 *   The name of the mailbox.
 * @param $message_sequence
 *   The message or sequence of messages to remove a flag for.
 * @param $flags
 *   The type of flag to remove. Possible flags:
 *    *\\Seen
 *    *\\Answered
 *    *\\Flagged
 *    *\\Deleted
 *    *\\Draft
 *   May be a single line seperated list of flags.
 * @param $account
 *   The account the message belongs to.
 */
function basic_webmail_unmark_message($mailbox_name, $uid, $flags, $account = NULL) {
  if ($imap_resource = basic_webmail_connection_load($mailbox_name, $account)) {
    $message_number = basic_webmail_msgno($mailbox_name, $uid, $account);
    if (imap_clearflag_full($imap_resource, $message_number, $flags)) {
      basic_webmail_message_load($mailbox_name, $uid, FALSE, $account, TRUE);
      return TRUE;
    }
    else {
      watchdog('basic_webmail', 'There was an error marking message @message_sequence as !flags: !last_error', array('!last_error' => imap_last_error(), '@message_sequence' => $message_sequence, '!flags' => str_replace('/', '', $flags)), WATCHDOG_ERROR);
    }
  }
  return FALSE;
}



/**
 * Converts UTF8 encoded strings.
 *
 * This function is necessary, because there is a bug in some versions of PHP
 * that returns strings converted with the imap_utf8() function in all upper
 * case letters.  http://bugs.php.net/bug.php?id=44098
 *
 * This function was taken from here and modified:
 * http://us2.php.net/manual/en/function.imap-utf8.php#92026
 */
function basic_webmail_decode_mime_str($string, $char_set = 'UTF-8') {
  $new_string = '';
  $elements = imap_mime_header_decode($string);

  for($i = 0; $i < count($elements); $i++) {
    if ($elements[$i]->charset == 'default') {
          $elements[$i]->charset = 'iso-8859-1';
    }

    $new_string .= iconv($elements[$i]->charset, $char_set, $elements[$i]->text);
  }

  return $new_string;
}