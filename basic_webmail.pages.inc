<?php
require_once dirname(__FILE__) . '/basic_webmail.api.inc';

/**
 * Display the listing of messages.
 *
 * @param folder
 *   The name of the folder to open. If no folder is specified, the default
 *   of 'INBOX' is used.
 *
 * @return
 *   The formatted message listing.
 */
function basic_webmail_mail_list($mailbox_name = 'INBOX') {
  $output = '';

  // Set the title of the page to the current mailbox, if there is one.
  if (!basic_webmail_mailbox_exists($mailbox_name)) {
    if (basic_webmail_subscribe_to_mailboxes() && !basic_webmail_mailbox_exists($mailbox_name)) {
      drupal_set_message(t('Unable to retrieve mailbox.'), 'error');
      return drupal_not_found();
    }
  }

  if ($info = basic_webmail_get_mailbox_info($mailbox_name)) {
    global $pager_page_array, $pager_total;
    $pager_page_array[0] = !empty($_GET['page']) ? $_GET['page'] : 0;
    $pager_total[0] = ceil($info->messages / variable_get('basic_webmail_messages_per_page', 25));
    $output .= drupal_get_form('basic_webmail_mail_list_form', $mailbox_name);
    $output .= theme('pager', NULL, variable_get('basic_webmail_messages_per_page', 25), 0);
    if ($info->unseen) {
      drupal_set_title(drupal_get_title() . ' ' . t('(@num unread)', array('@num' => $info->Unread)));
    }
  }
  else {
    $output .= t('No messages exist in this folder.');
  }
  return $output;
}


/**
 * Construct the form for the message listing w/checkboxes.
 *
 * @return
 *   The completed form definition.
 */
function basic_webmail_mail_list_form(&$form_state, $mailbox_name = 'INBOX') {
  // Load the custom CSS file.
  drupal_add_css(drupal_get_path('module', 'basic_webmail') . '/basic_webmail.css');

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  $limit = variable_get('basic_webmail_messages_per_page', 25);
  $messages = basic_webmail_list_messages($mailbox_name, $limit * $page, $limit, 'DESC');
  $count = basic_webmail_get_message_count();

  $leaf = theme('image', 'misc/menu-leaf.png');

  foreach ($messages as $message) {
    // Checkbox for actions.
    $messages[$message['uid']] = '';
    $form['message_unread'][$message['uid']] = array(
      '#value' => $message['unseen'] ? $leaf : '',
      '#prefix' => '<div class="column-center">',
      '#suffix' => '</div>',
    );
    $form['attachment_exists'][$message['uid']] = array(
      '#value' =>  $message['attachments'] ? $leaf : '',
      '#prefix' => '<div class="column-center">',
      '#suffix' => '</div>',
    );
    $form['message_answered'][$message['uid']] = array(
      '#value' => $message['answered'] ? $leaf : '',
      '#prefix' => '<div class="column-center">',
      '#suffix' => '</div>',
    );

    $subject = l($message['display_subject'], $message['path']);
    if ($message['unseen']) {
      $subject = '<strong>' . $subject . '</strong>';
    }

    $form['message_subject'][$message['uid']] = array(
      '#value' => $subject
    );
    $form['from_address'][$message['uid']] = array(
      '#value' => theme('basic_webmail_mail_link', $message['from'][0]['display_name'], $message['from'][0]['mail']),
    );
    $form['message_date'][$message['uid']] = array(
      '#value' => $message['date_formated'],
    );
  }

  $form['messages'] = array(
    '#type' => 'checkboxes',
    '#options' => $messages
  );

  $form['operation'] = array(
    '#type' => 'select',
    '#title' => t('With checked'),
    '#default_value' => 'read',
    '#options' => array(
      'delete' => t('Delete'),
      'read' => t('Mark read'),
      'unread' => t('Mark unread'),
      'copy_to' => t('Copy'),
      'move_to' => t('Move'),
    ),
    '#prefix' => '<div class="container-inline">',
  );

  $form['folder_name'] = array(
    '#type' => 'select',
    '#title' => t('The folder to copy or move to'),
    '#options' => _basic_webmail_get_mailbox_options(),
  );
  unset($form['folder_name']['#options'][$mailbox_name]);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#suffix' => '</div>',
  );

  $form['#folder'] = $mailbox_name;

  return $form;
}

/**
 * Return an array of mailbox options to use in form elements.
 */
function _basic_webmail_get_mailbox_options() {
  $options = array();
  foreach (basic_webmail_get_mailboxes_subscribed() as $mailbox_name) {
    $options[$mailbox_name] = basic_webmail_mailbox_title($mailbox_name);
  }
  return $options;
}

/**
 * Perform an action on a group of messages.
 *
 * @param form_id
 *   The form ID of the passed form.
 * @param $form_state
 *   The form values of the passed form.
 */
function basic_webmail_mail_list_form_submit($form, &$form_state) {
  $operation = $form_state['values']['operation'];
  // Filter out unselected messages.
  $messages = array_filter($form_state['values']['messages']);
  $mailbox_name = $form['#folder'];

  if ($messages = array_filter($form_state['values']['messages'])) {
    $effected = 0;
    foreach ($messages as $uid) {
      switch ($operation) {
        case 'delete':
          if (!basic_webmail_message_delete($mailbox_name, $uid, NULL, FALSE, FALSE)) {
            drupal_set_message(t('Error encountered when deleting a message. Please try later.'), 'error', FALSE);
          }
          else {
            $effected++;
          }
          break;
        case 'copy_to':
        case 'move_to':
          $function =  $operation == 'copy_to' ? 'basic_webmail_message_copy' : 'basic_webmail_message_move';
          if (!$function($mailbox_name, $uid, $form_state['values']['folder_name'], NULL, FALSE)) {
            drupal_set_message(t('Error encountered when moving a message. Please try later.'), 'error', FALSE);
          }
          else {
            $effected++;
          }
          break;
        case 'read':
        case 'unread':
          $function =  $operation == 'read' ? 'basic_webmail_mark_message' : 'basic_webmail_unmark_message';
          if (!$function($mailbox_name, $uid, '\\Seen')) {
            drupal_set_message(t('Error changing status of message. Please try later.'), 'error', FALSE);
          }
          else {
            $effected++;
          }
        break;
      }
    }
    $options = array(
      '@num' => $effected,
    );
    switch ($operation) {
      case 'delete':
        basic_webmail_expunge_mailbox($mailbox_name);
        if ($effected) {
          drupal_set_message($mailbox_name == 'INBOX.Trash' ? t('@num messages permanently deleted.', $options) : t('@num messages moved to trash.', $options));
        }
        break;
      case 'copy_to':
      case 'move_to':
        basic_webmail_expunge_mailbox($mailbox_name);
        basic_webmail_expunge_mailbox($form_state['values']['folder_name']);
        if ($effected) {
          drupal_set_message($operation == 'copy_to' ? t('@num messages successfully copied.', $options) : t('@num messages successfully moved.', $options));
        }
        break;
      case 'read':
      case 'unread':
        if ($effected) {
          drupal_set_message($operation == 'read' ? t('@num messages successfully marked as read.', $options) : t('@num messages successfully marked as unread.', $options));
        }
        break;
    }
  }
}

/**
 * Display the message.
 *
 * @param $uid
 *   The ID of the message to delete.
 * @param $folder
 *   The name of the folder the message is in. If no folder is specified, it
 *   defaults to 'INBOX'.
 *
 * @return
 *   The formatted message.
 */
function basic_webmail_message_page($mailbox_name, $uid) {
  $message = basic_webmail_message_load($mailbox_name, $uid, TRUE);
  // Set the title of the page.
  drupal_set_title(check_plain($message['subject']));
  $output = '<table>';

  $fields = array(
    'from' => t('From'),
    'to' => t('To'),
    'cc' => t('CC'),
    'bcc' => t('BCC'),
  );

  foreach ($fields as $key => $display) {
    if ($message[$key]) {
      $emails = array();
      foreach ($message[$key] as $email) {
        $emails[] = theme('basic_webmail_mail_link', $email['mail'], $email['display_name']);
      }
      $output .= '<tr><td>' . $display. ':</td><td>' . implode(', ', $emails) . '</td></tr>';
    }
  }

  // Retrieve and display the date of the message.
  $output .= '<tr><td>Date:</td><td>' . $message['date_formated'] . '</td></tr>';

  // Close out the header.
  $output .= '</table>';
  $display = arg(4) == 'html' ? 'HTML' : 'PLAIN';
  $function = $display == 'HTML' ? 'filter_xss' : 'check_plain';
  $display_switch_link = FALSE;
  foreach ($message['message_body_parts'] as $part) {
    if ($part['type'] == $display) {
      if ($display == 'HTML') {
        $display_parts[] = check_markup($part['body'], variable_get('basic_webmail_filter_format', FILTER_FORMAT_DEFAULT));
      }
      else {
        $display_parts[] = _filter_autop(check_plain($part['body']));
      }
    }
    else {
      $display_switch_link = TRUE;
    }
  }
  if ($display_switch_link) {
    if ($display == 'HTML') {
      $output .= '<div class="basic-webmail-view-as-text warning">' . l(t('View as plain text.'), 'basic_webmail/message/' . $mailbox_name . '/' . $uid) . '</div>';
    }
    else {
      $output .= '<div class="basic-webmail-view-as-html error">' . t('Message is being displayed as plain text.') . ' ' . l(t('View as HTML.'), 'basic_webmail/message/' . $mailbox_name . '/' . $uid . '/html') . ' ' . t('Only view HTML messages from those you trust.') . '</div>';
    }
  }

  $output .= $display_parts ?  implode('<hr width="75%">', $display_parts) : t('No body.');

  if ($message['attachments_links']) {
    $output .= '<hr><strong>' . format_plural(count($message['attachments_links']), 'Attachment', 'Attachments') . '</strong>:';
    $output .= implode('&nbsp; -- &nbsp;', $message['attachments_links']);
  }

  return $output;
}

/**
 * Display the message attachment.
 *
 * @param uid
 *   The ID of the message to delete.
 * @param folder
 *   The name of the folder the message is in. If no folder is specified, it
 *   defaults to 'INBOX'.
 * @param $part_id
 *   The ID of the part to be displayed..
 *
 * @return
 *   The formatted message.
 */
function basic_webmail_message_attachment_page($mailbox_name, $message_number, $part_id) {
  // TODO move most of this logic into an api function.
  $message = basic_webmail_message_load($mailbox_name, $message_number, TRUE);

  if (empty($message['parts_list'][$part_id][1])) {
    drupal_set_message(t('Unable to retrieve attachment at this time. Please try again later.'));
    return MENU_NOT_FOUND;
  }
  $part_structure = $message['parts_list'][$part_id][1];

  if (!empty($part_structure->dparameters[0]->value)){
    $file_name = $part_structure->dparameters[0]->value;
  }
  else {
    $file_name = $part_structure->description;
  }

  $file_data = _basic_webmail_mail_fetchpart(basic_webmail_connection_load($mailbox_name), $message_number, $part_id);
  if ($part_structure->encoding == 3) {
    // Decode if base64.
    $file_data = base64_decode($file_data);
  }
  elseif ($part_structure->encoding == 4) {
    $file_data = quoted_printable_decode($file_data);
  }

  if (ob_get_level()) {
    ob_end_clean();
  }

  if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) {
    drupal_set_header('Cache-Control: private');
    drupal_set_header('Pragma: private');
  }

  drupal_set_header('Content-Description: File Transfer');
  drupal_set_header('Content-Type: ' . file_get_mimetype($file_name));
  drupal_set_header('Content-Disposition: attachment; filename=' . preg_replace('/\r?\n(?!\t| )/', '', $file_name));

  flush();
  echo $file_data;
}

/**
 * Post a message delete confirmation form.
 *
 * @param message
 *   The loaded message number.
 *
 * @return
 *   The results of the confirmation question.
 */
function basic_webmail_delete_message_form($form_state, $mailbox_name, $uid) {
  $message = basic_webmail_message_load($mailbox_name, $uid, TRUE);
  // Fill in the form fields.
  $form['#uid'] = $message['uid'];
  $form['#folder'] = $message['mailbox_name'];

  // Fill in the rest of the confirm_form() variables.
  $question = t('Delete message?');
  $path = 'basic_webmail/message/' . $message['mailbox_name'] . '/' . $message['uid'];
  $description = t("Are you sure you want to delete the message from @from_address, sent on @message_date with the subject of '@message_subject'?", array('@from_address' =>  $message['from'][0]['display_name'], '@message_date' =>  $message['date_formated'], '@message_subject' => $message['subject']));

  // Call the function with the data.
  return confirm_form($form, t('Delete message?'), $path, $description, t('Yes'), t('No'), 'confirm_message_deletion');
}


/**
 * Delete a specified message.
 *
 * @param form_id
 *   The ID of the form created in the form generation function.
 * @param form_values
 *   The contents of the form fields.
 */
function basic_webmail_delete_message_form_submit($form, &$form_state) {

  if (basic_webmail_message_delete($form['#folder'], $form['#uid'])) {
    drupal_set_message(t('The message was deleted.'));
  }
  else {
    drupal_set_message(t('There was an error deleting the message. Please try again later'), 'error');
  }

  $form_state['redirect'] = 'basic_webmail/' . $form['#folder'];
}

/**
 * Post a message copy form.
 *
 * @param message
 *   The message array.
 *
 * @return
 *   The results of the confirmation question.
 */
function basic_webmail_copy_message_form($form_state, $mailbox_name, $uid) {
  $message = basic_webmail_message_load($mailbox_name, $uid, TRUE);
  $form = array();

  $form['destination_folder'] = array(
    '#type' => 'select',
    '#title' => t('Folder'),
    '#description' => t('The folder to which you want to copy the message'),
    '#options' => _basic_webmail_get_mailbox_options(),
  );
  unset($form['destination_folder']['#options'][$message['mailbox_name']]);

  $form['#uid'] = $message['uid'];
  $form['#folder'] = $message['mailbox_name'];

  // Fill in the rest of the confirm_form() variables.
  $path = 'basic_webmail/message/' . $message['mailbox_name'] . '/' . $message['uid'];
  $description = t("Copy message from @from_address, sent on @message_date with the subject of '@message_subject'?", array('@from_address' =>  $message['from'][0]['display_name'], '@message_date' =>  $message['date_formated'], '@message_subject' => $message['subject']));
  return confirm_form($form, t('Copy message?'), $path, $description, t('Copy'), t('Cancel'), 'confirm_message_copy');
}


/**
 * Copy a specified message.
 *
 * @param form_id
 *   The ID of the form created in the form generation function.
 * @param form_values
 *   The contents of the form fields.
 */
function basic_webmail_copy_message_form_submit($form, &$form_state) {
  if (basic_webmail_message_copy($form['#folder'], $form['#uid'], $form_state['values']['destination_folder'])) {
    drupal_set_message(t('The message was copied.'));
  }
  else {
    drupal_set_message(t('There was an error copying message #!uid.'), 'error');
  }
  $form_state['redirect'] = 'basic_webmail/' . $form_state['values']['destination_folder'];
}


/**
 * Post a message copy form.
 *
 * @param message
 *   The message array.
 *
 * @return
 *   The results of the confirmation question.
 */
function basic_webmail_move_message_form($form_state, $mailbox_name, $uid) {
  $message = basic_webmail_message_load($mailbox_name, $uid, TRUE);
  $form = array();

  $form['destination_folder'] = array(
    '#type' => 'select',
    '#title' => t('Folder'),
    '#description' => t('The folder to which you want to move the message'),
    '#options' => _basic_webmail_get_mailbox_options(),
  );
  unset($form['destination_folder']['#options'][$message['mailbox_name']]);

  $form['#uid'] = $message['uid'];
  $form['#folder'] = $message['mailbox_name'];

  // Fill in the rest of the confirm_form() variables.
  $path = 'basic_webmail/message/' . $message['mailbox_name'] . '/' . $message['uid'];
  $description = t("Move message from @from_address, sent on @message_date with the subject of '@message_subject'?", array('@from_address' =>  $message['from'][0]['display_name'], '@message_date' =>  $message['date_formated'], '@message_subject' => $message['subject']));
  return confirm_form($form, t('Move message?'), $path, $description, t('Move'), t('Cancel'), 'confirm_message_copy');
}


/**
 * Copy a specified message.
 *
 * @param form_id
 *   The ID of the form created in the form generation function.
 * @param form_values
 *   The contents of the form fields.
 */
function basic_webmail_move_message_form_submit($form, &$form_state) {
  if (basic_webmail_message_move($form['#folder'], $form['#uid'], $form_state['values']['destination_folder'])) {
    drupal_set_message(t('The message was moved.'));
  }
  else {
    drupal_set_message(t('There was an error moving message.'), 'error');
  }
  $form_state['redirect'] = 'basic_webmail/' . $form_state['values']['destination_folder'];
}

/**
 * Formats a an email for a to/etc/ line.
 */
function _basic_webmail_format_email($display, $mail) {
  return $display == $mail ? $mail : $display . ' <' . $mail . '>';
}

/**
 * Construct the form for conposing an e-mail message.
 *
 * @param send_mail_type
 *   Whether the message is new, a reply or a forward. Possible values:
 *   - "new": All form fields are left blank.
 *   - "reply": The To form field is filled in with the from address of the
 *     original message. The Subject and Body form fields are filled in with
 *     the corresponding fields from the original message.
 *   - "replytoall": The To form field is filled in with the from address of
 *     the original message. The CC form field is filled in with all of the
 *     addresses in the to and cc fields of the original message. The Subject
 *     and Body form fields are filled in with the corresponding fields from
 *     the original message.
 *   - "forward": The To, CC and BCC form fields are blank. The Subject and
 *     Body form fields are filled in with the corresponding fields from the
 *     original message.
 *   - "continue": The contents of the form are filled in by the message in
 *     the Drafts folder that is being continued.
 *   - An e-mail address: The To form field gets filled in with the value in
 *     $send_mail_type, and all other form fields are left blank.
 * @param send_mail_message_number
 *   The ID of the message being replied to or forwarded, blank otherwise.
 *
 * @return
 *   The completed form definition.
 */
function basic_webmail_send_mail_form(&$form_state, $send_mail_type = 'new', $mailbox_name = NULL, $uid = NULL) {
  if ($mailbox_name && $uid) {
    $message = basic_webmail_message_load($mailbox_name, $uid, TRUE);
  }
  $form = array();
  $values = array(
    'to' => '',
    'from' => '',
    'cc' => '',
    'bcc' => '',
    'subject' => '',
    'body' => '',
  );

  // Allow some prepopulating via url.
  foreach ($values as $key => $val) {
    if (!empty($_GET[$key])) {
      $values[$key] = $_GET[$key];
    }
  }

  // Check to see if this is a reply or forward.
  if ($send_mail_type != 'new' && $message) {
    if ($send_mail_type == 'reply') {
      drupal_set_title(t('Reply to @subject', array('@subject' => $message['subject'])));
    }
    elseif($send_mail_type == 'replytoall') {
      drupal_set_title(t('Reply all to @subject', array('@subject' => $message['subject'])));
    }
    elseif ($send_mail_type == 'forward') {
      drupal_set_title(t('Forward @subject', array('@subject' => $message['subject'])));
    }
    elseif ($send_mail_type == 'continue') {
      drupal_set_title(t('Continue @subject', array('@subject' => $message['subject'])));
    }

    $form['#uid'] = $message['uid'];
    $form['#mailbox_name'] = $message['mailbox_name'];
    $form['#message_id'] = $message['id'];

    $parts = array();
    foreach ($message['message_body_parts'] as $part) {
      if ($part['type'] == 'text') {
        $parts[] = _filter_autop(check_plain($part['body']));
      }
    }

    if ($send_mail_type == 'reply' || $send_mail_type == 'replytoall') {
      $values['subject']  = t('Re') . ': ' . $message['subject'];
      $values['to'] = _basic_webmail_format_email($message['from'][0]['display_name'], $message['from'][0]['mail']);
      $values['body'] = "\n\n\nOn " .  $message['date_formated'] . ', ' . $values['to'] . " wrote:\n";
      $values['body'] .= $message['message_PLAIN'] ? '> ' . str_replace("\n", "\n> ", $message['message_PLAIN']) : '';
      $values['body'] .= "\n";

      // Add the sender's signature, if one exists.
      global $user;
      // Set defaults based on $send_mail_type.
      if ($send_mail_type == 'replytoall') {
        foreach ($message['to'] as $email) {
          $values['cc'] .= _basic_webmail_format_email($email['display_name'], $email['mail']) .', ';
        }

        foreach ($message['cc'] as $email) {
          $values['cc'] .= _basic_webmail_format_email($email['display_name'], $email['mail']) .', ';
        }
      }
    }
    elseif ($send_mail_type == 'forward') {
        // Set the subject of the message.
        $values['subject'] = t('Fw') . ': ' . $message['subject'];

        // Set the body of the message.
        $values['body'] = "\n\n\n------- Forwarded Message\n\n";
        $values['body'] .= $message['message_PLAIN'];
        $values['body'] .= "\n\n------- End of Forwarded Message\n\n";
    }
    elseif ($send_mail_type == 'continue') {
      // Set the "To" of the message.
      $values['to'] = $message['header']->toaddress;
      // Set the cc of the message.
      $values['cc'] = $message['header']->ccaddress;
      // Set the bcc of the message.
      $values['bcc'] = $message['header']->bccaddress;
      // Set the subject of the message.
      $values['subject'] = $message['subject'];
      // Fill in the body of the message.
      $values['body'] = $message['message_PLAIN'];
    }
    if ($send_mail_type != 'continue' && $user->signature) {
      $values['body'] .= "\n-- \n" . $user->signature;
    }
  }
  elseif ($send_mail_type != 'new') {
    $values['to'] = $send_mail_type;
  }

  $form['to'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#default_value' => $values['to'],
    '#maxlength' => 1024,
    '#autocomplete_path' => 'basic_webmail/autocomplete',
    '#description' => t('One or more recepients of this message. Separate the addresses with a comma (,).'),
    '#required' => TRUE,
  );

  $form['cc'] = array(
    '#type' => 'textfield',
    '#title' => t('CC'),
    '#default_value' => $values['cc'],
    '#maxlength' => 1024,
    '#autocomplete_path' => 'basic_webmail/autocomplete',
    '#description' => t('One or more e-mail addresses to receive a carbon copy. Separate the addresses with a comma (,).'),
  );
  $form['bcc'] = array(
    '#type' => 'textfield',
    '#title' => t('BCC'),
    '#default_value' => $values['bcc'],
    '#maxlength' => 1024,
    '#autocomplete_path' => 'basic_webmail/autocomplete',
    '#description' => t('One or more e-mail addresses to receive a blind carbon copy. These e-mail addresses will not be visible in the message. Separate the addresses with a comma (,).'),
  );
  $form['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $values['subject'],
    '#maxlength' => 1024,
  );
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $values['body'],
    '#rows' => 15,
  );
  // If any exists, include attachments from message as checkboxes here.
  if (!empty($attachments)) {
    $form['basic_webmail_attachments'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Attachments from original message'),
      '#options' => $attachments,
      '#description' => t('Check any attachments from the original you want to include in this message.'),
    );
  }

  for ($i = 1; $i <= variable_get('basic_webmail_number_attachments', 3); $i++) {
    $form['basic_webmail_attachment_'. $i] = array(
      '#type' => 'file',
      '#title' => t('Attachment #!number', array('!number' => $i)),
    );
  }

  $form['#mail_type'] = $send_mail_type;
  $form['#validate'][] = 'basic_webmail_send_mail_form_validate';

  $form['draft'] = array(
    '#type' => 'submit',
    '#value' => t('Save as Draft.'),
    '#submit' => array('basic_webmail_send_mail_form_draft_submit'),
  );
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  $form['#attributes']['enctype'] = 'multipart/form-data';

  return $form;
}

/**
 * Validate send mail attachements.
 */
function basic_webmail_send_mail_form_validate($form, &$form_state) {
  for ($i = 1; $i <= variable_get('basic_webmail_number_attachments', '3'); $i++) {
    if (!empty($_FILES['files']['name']['basic_webmail_attachment_'. $i]) && !basic_webmail_validate_file('basic_webmail_attachment_'. $i)) {
      form_set_error('basic_webmail_attachment_' . $i);
    }
  }
}

/**
 * Send the message.
 *
 * @param form_id
 *   The form ID of the passed form.
 * @param $form_state
 *   The form values of the passed form.
 */
function basic_webmail_send_mail_form_submit($form, &$form_state) {
  global $user;

  $processed = _basic_webmail_send_mail_form_submit($form, $form_state);
  // Initialize variables.
  $sent_envelope = $processed['envelope'];
  $sent_body = $processed['body'];
  $params = $processed['params'];

  global $user;
  $to_account = user_load(array('name' => $sent_envelope['to']));
  $from_account = user_load($user->uid);

  // If using account info for IMAP, send mail to usernames, not emails
  if (variable_get('basic_webmail_account_info', 1)){
    $send_successful = basic_webmail_send_message($sent_envelope, $sent_body, 'INBOX', $to_account, $from_account);
  }
  else{
    $send_successful = drupal_mail('basic_webmail', 'message', $sent_envelope['to'], language_default(), $params, $sent_envelope['from']);
  }

  if ($send_successful) {
    // If this is a reply, mark the replied to message as answered.
    if ($form['#mail_type'] == 'reply' || $form['#mail_type'] == 'replytoall') {
      basic_webmail_mark_message($form['#mailbox_name'], $form['#uid'], '\\Answered');
    }

    // Copy the message to the INBOX.Sent folder, marked as read.
   // Create the message string to store.
    basic_webmail_send_message($sent_envelope, $sent_body, 'INBOX.Sent', $to_account, $from_account, array('flags' => '\\Seen'));
    // Report to the user.
    drupal_set_message(t('Your message was successfully sent.'));

    // Where we go after the message is sent.
    if (!empty($form['#uid'])) {
      // The original message, if there was one.
      $form_state['#redirect'] = 'basic_webmail/message/'. $form['#mailbox_name'] .'/'. $form['#uid'];
    }
    else {
      // The message list otherwise.
      $form_state['#redirect'] = 'basic_webmail';
    }
  }
  else {
    // Report to the user.
    drupal_set_message(t('There was an error sending your message.'), 'error');
  }
}


/**
 * Send the message.
 *
 * @param form_id
 *   The form ID of the passed form.
 * @param $form_state
 *   The form values of the passed form.
 */
function basic_webmail_send_mail_form_draft_submit($form, &$form_state) {
  global $user;
  $processed = _basic_webmail_send_mail_form_submit($form, $form_state);;

  $sent_envelope = $processed['envelope'];
  $sent_body = $processed['body'];
  $params = $processed['params'];

  // If relavent, delete old message
  if ($form['#mail_type'] == 'continue') {
    basic_webmail_message_delete($form['#mailbox_name'], $form['#uid']);
  }

  // Save the new message.
  if (basic_webmail_send_message($sent_envelope, $sent_body, 'INBOX.Drafts')) {
    drupal_set_message(t('Message saved to draft folder'));
    $form_state['redirect'] = 'basic_webmail/INBOX.Drafts';
  }
  else {
    $form_state['rebuild'] = FALSE;
    drupal_set_message(t('There was an error saving your message.'), 'error');
  }
}

/**
 * Helper function to compose the message information before sending.
 */
function _basic_webmail_send_mail_form_submit($form, $form_state) {
  global $user;

  // Initialize variables.
  $sent_envelope = array();
  $sent_body = array();
  $send_mail_headers = array();
  $send_mail_body = '';


  // Set the From e-mail address and associated header information.
  $sent_envelope['from'] = $sent_envelope['return_path'] = $sent_envelope['reply_to'] = $send_mail_headers['Sender'] = $send_mail_headers['Return-Path'] = $user->basic_webmail_username;

  $is_reply = $form['#mail_type'] == 'reply' || $form['#mail_type'] == 'replytoall';
  if ($is_reply) {
    $sent_envelope['in_reply_to'] = $send_mail_headers['in_reply_to']  = $form['#message_id'];
  }


  // Set the CC and BCC fields, if they exist.
  foreach (array('to', 'cc', 'bcc', 'subject') as $key) {
    if (!empty($form_state['values'][$key])) {
      $sent_envelope[$key] = $send_mail_headers[lcfirst($key)] = $form_state['values'][$key];
    }
  }

  $attached_files = array();
  // Check for the exsistance of added attachments.
  for ($i = 1; $i <= variable_get('basic_webmail_number_attachments', '3'); $i++) {
    if (!empty($_FILES['files']['tmp_name']['basic_webmail_attachment_'. $i])) {
      $attached_files[] = $i;
    }
  }

  // Create headers based on presence or absence of attachment.
  if ($attached_files) {
    // Inform that there is attachments.
    $sent_body[] = array(
      'type' => TYPEMULTIPART,
      'subtype' => 'mixed',
    );

    $unique_id = md5(uniqid(time()));
    $send_mail_headers['Content-Type'] = 'multipart/mixed; boundary="'. $unique_id .'"';
    $send_mail_body = "\n--$unique_id\n";
    $send_mail_body .= "Content-Type: text/plain; charset=UTF-8; format=flowed\n\n";


    // Add in the actual message
    $body_wrapped = wordwrap($form_state['values']['body']) ."\n\n\n";
    $sent_body[] = array(
      'type' => TYPETEXT,
      'subtype' => 'plain',
      'description' => 'message',
      'contents.data' => $body_wrapped,
    );
    $send_mail_body .= $body_wrapped;

    // Add any added attachments.
    foreach ($attached_files as $i) {
      $content_type = $_FILES['files']['type']['basic_webmail_attachment_'. $i];
      $attachment = $_FILES['files']['tmp_name']['basic_webmail_attachment_'. $i];
      $name = $_FILES['files']['name']['basic_webmail_attachment_'. $i];
      $subkey = substr($content_type, 0, strpos($content_type, '/'));
      $type_key = 'TYPE' . strtoupper($subkey);
      $description = basename($name);
      $content = chunk_split(base64_encode(file_get_contents($attachment)));

      $sent_body[] = array(
        'type' => defined($type_key) ? constant($type_key) : TYPEAPPLICATION,
        'encoding' => ENCBASE64,
        'subtype' => $subkey,
        'description' => $description,
        'disposition.type' => 'attachment',
        'disposition' => array('filename' => $description),
        'type.parameters' => array('name' => $description),
        'contents.data' => $content,
      );

      $send_mail_body .= "--$unique_id\n";
      $send_mail_body .= "Content-Type: ". $content_type ."; name=\"". $description ."\"\n";
      $send_mail_body .= "Content-Transfer-Encoding: base64\n";
      $send_mail_body .= "Content-Disposition: attachment; filename=\"" . $description . "\"\n\n";
      $send_mail_body .= $content;
      $send_mail_body .= "\n\n";
    }

    // Add the final boundary to the body.
    $send_mail_body .= "--$unique_id--\n\n";
  }
  // No attachments exist.
  else {
    // Add in the actual message
    $body_wrapped = wordwrap($form_state['values']['body']) ."\n\n\n";
    $sent_body[] = array(
      'type' => TYPETEXT,
      'subtype' => 'plain',
      'description' => 'message',
      'contents.data' => $body_wrapped,
    );
    $send_mail_body .= $body_wrapped;
  }
  $sent_envelope['date'] = date('r');

  return array(
    'body' => $sent_body,
    'envelope' => $sent_envelope,
    'params' => array(
      'subject' => $form_state['values']['subject'],
      'body' => $send_mail_body,
      'headers' => $send_mail_headers,
    ),
  );
}


/**
 * Automatically fills in e-mail addresses of the current users.
 */
function basic_webmail_autocomplete($string = '') {
  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  $array = drupal_explode_tags($string);
  // Fetch last tag
  $last_string = trim(array_pop($array));
  $matches = array();

  if ($last_string != '') {
    // Search the users table for all e-mail addresses which are similar to
    // the last item in the list.
    $search_field = variable_get('basic_webmail_account_info', 1) ? 'name' : 'mail';
    $result = db_query_range("SELECT %s FROM {users} WHERE LOWER(%s) LIKE LOWER('%%%s%%') AND status=1", $search_field, $search_field, $last_string, 0, 10);

    // Convert what's left of the array to a string.
    $prefix = count($array) ? implode(', ', $array) .', ' : '';

    // Iterate through the list of similar e-mail addresses.
    while ($mail = db_result($result)) {
      $matches[$prefix . $mail] = $mail;
    }
  }

  // Send the results back.
  drupal_json($matches);
}


/**
 * Validates the file but does not save it.
 *
 * @see file_save_upload().
 */
function basic_webmail_validate_file($source) {
  global $user;

  // If a file was uploaded, process it.
  if (isset($_FILES['files']) && $_FILES['files']['name'][$source]) {
    // Check for file upload errors and return FALSE if a
    // lower level system error occurred.
    switch ($_FILES['files']['error'][$source]) {
      // @see http://php.net/manual/en/features.file-upload.errors.php
      case UPLOAD_ERR_OK:
        break;
      case UPLOAD_ERR_INI_SIZE:
      case UPLOAD_ERR_FORM_SIZE:
        drupal_set_message(t('The file %file could not be saved, because it exceeds %maxsize, the maximum allowed size for uploads.', array('%file' => $source, '%maxsize' => format_size(file_upload_max_size()))), 'error');
        return 0;
      case UPLOAD_ERR_PARTIAL:
      case UPLOAD_ERR_NO_FILE:
        drupal_set_message(t('The file %file could not be saved, because the upload did not complete.', array('%file' => $source)), 'error');
        return 0;
      default:
        drupal_set_message(t('The file %file could not be saved. An unknown error has occurred.', array('%file' => $source)), 'error');
        return 0;
    }

    // file_save_upload has this check at the begining, but results in no error message for some of above.
    if (!is_uploaded_file($_FILES['files']['tmp_name'][$source])) {
      drupal_set_message(t('The file %file could not be saved. An unknown error has occurred.', array('%file' => $source)), 'error');
      return 0;
    }

    // Build the list of non-munged extensions.
    // @todo: this should not be here. we need to figure out the right place.
    $extensions = '';
    foreach ($user->roles as $rid => $name) {
      $extensions .= ' ' . variable_get("upload_extensions_$rid",
      variable_get('upload_extensions_default', 'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp'));
    }

    // Begin building file object.
    $file = new stdClass();
    $file->filename = file_munge_filename(trim(basename($_FILES['files']['name'][$source]), '.'), $extensions);
    $file->filepath = $_FILES['files']['tmp_name'][$source];
    $file->filemime = file_get_mimetype($file->filename);
    $file->source = $source;
    $file->filesize = $_FILES['files']['size'][$source];

    // Call the validation functions.
    $errors = array();
    if (variable_get('basic_webmail_attachment_extentions', '')) {
      $errors = file_validate_extensions($file, variable_get('basic_webmail_attachment_extentions', ''));
    }
    if (variable_get('basic_webmail_attachment_size', 0)) {
      $errors = array_merge($errors, file_validate_size($file, variable_get('basic_webmail_attachment_size', 0), 0));
    }

    // Check for validation errors.
    if (!empty($errors)) {
      $message = t('The selected file %name could not be used.', array('%name' => $file->filename));
      if (count($errors) > 1) {
        $message .= '<ul><li>' . implode('</li><li>', $errors) . '</li></ul>';
      }
      else {
        $message .= ' ' . array_pop($errors);
      }
      form_set_error($source, $message);
      return 0;
    }

    return $file;
  }
  return 0;
}