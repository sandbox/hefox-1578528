<?php
/**
 * @file
 * Provide views data and handlers for basic_webmail.module
 */

/**
 * Implementation of hook_views_data().
 */
function basic_webmail_views_data() {
  $data['users']['basic_webmail_link'] = array(
    'group' => t('Basic Webmail'),
    'title' => t('Send message'),
    'field' => array(
      'title' => t('Send message link'),
      'help' => t('Displays a link to send a message to a user.'),
      'handler' => 'views_handler_field_basic_webmail_link',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function basic_webmail_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'basic_webmail') . '/views',
    ),
    'handlers' => array(
      // field handlers
      'views_handler_field_basic_webmail_link' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
