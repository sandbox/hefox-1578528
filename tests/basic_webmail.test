<?php


/**
 * Test contained links in administration menu.
 */
class BasicWebmailApiTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Basic Webmail API'),
      'description' => t('Test the api functions of basic webmail.'),
      'group' => t('Basic webmail'),
    );
  }

  function setUp() {
    parent::setUp('basic_webmail');
    module_load_include('api.inc', 'basic_webmail');
    // Load a git ignored file with connection info.
    module_load_include('inc', 'basic_webmail', 'tests/basic_webmail.config');
  }

  /**
   * Test link contents.
   */
  function testBasic() {
    $this->assertTrue(basic_webmail_connection_load(), 'Connection made with server string: ' . basic_webmail_get_server_string());
    basic_webmail_subscribe_to_mailboxes();
    $subscribed_mailboxes = basic_webmail_get_mailboxes_subscribed();
    foreach (array_keys(basic_webmail_mailboxes()) as $box) {
      $this->assertTrue(in_array($box, $subscribed_mailboxes), 'User successfully subscribed to mailbox ' . $box);
    }

    // Test creation of mailbox.
    $name = 'test_mailbox_' . time();
    $this->assertTrue(basic_webmail_create_mailbox($name), 'Test mailbox successfully created: ' . $name);
    $this->assertTrue(basic_webmail_mailbox_exists($name), 'New mailbox appears in list of mailboxes: ' . $name);
    $this->assertTrue(basic_webmail_delete_mailbox($name), 'Test mailbox successfully deleted: ' . $name);

    // Test subscription creation.
    $name = 'test_mailbox_2_' . time();
    $this->assertTrue(basic_webmail_mailbox_subscribe($name), 'Test mailbox successfully created and subscribed to: ' . $name);

    $subscribed_mailboxes = basic_webmail_get_mailboxes_subscribed();
    $all_mailboxes = basic_webmail_get_mailboxes();
    $this->assertTrue(in_array($name, $subscribed_mailboxes) && in_array($name, $all_mailboxes), 'User is correctly subscribed to test mailbox: ' . $name);

    $this->assertTrue(basic_webmail_delete_mailbox($name), 'Test mailbox successfully deleted: ' . $name);

    // Try sending a message.
    $envelope = array(
      'from' => 'example@example.com',
      'subject' => 'Test message ' . time(),
    );
    $body = array(
      array(
        'type' => TYPETEXT,
        'subtype' => 'plain',
        'description' => 'description',
        'contents.data' => "contents.data3\n\n\n\t",
      ),
    );

    $test_user = $this->drupalCreateUser();
    $test_user->basic_webmail_username = 'example@example.com';
    $this->assertTrue(basic_webmail_send_message($envelope, $body), 'Basic test message sent.');

    $subject = 'Hello, the time is ' . time();
    $this->assertTrue(basic_webmail_send_message_simple($subject, 'Good bye', $GLOBALS['user'], $test_user), 'Basic test message sent.');

    // Clear out the trash.
    foreach (basic_webmail_list_messages('INBOX.Trash', 0, 0) as $message) {
      basic_webmail_message_delete('INBOX.Trash', $message['message_number'], NULL, FALSE, FALSE);
    }

    // Delete the last message.
    $last_message = basic_webmail_last_message();
    $this->assertTrue(basic_webmail_message_delete('INBOX', $last_message['message_number']), 'Last message moved to trash.');

    // Now really delete it.
    $new_last_message = basic_webmail_last_message('INBOX.Trash');
    $this->assertTrue($new_last_message['header']->subject == $last_message['header']->subject, 'Last message appears in trash.');
    $this->assertTrue(basic_webmail_message_delete('INBOX.Trash', $new_last_message['message_number']), 'Last message deleted permanently.');
    $this->assertTrue(!basic_webmail_last_message('INBOX.Trash'), 'Trash is empty.');

    basic_webmail_connection_close();
  }
}

